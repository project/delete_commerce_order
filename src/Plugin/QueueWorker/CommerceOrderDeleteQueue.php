<?php

namespace Drupal\delete_commerce_order\Plugin\QueueWorker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes Order Delete.
 *
 * @QueueWorker(
 *   id = "commerce_delete_order",
 *   title = @Translation("Commerce Order Delete Queue"),
 *   cron = {"time" = 300}
 * )
 */
class CommerceOrderDeleteQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new CommerceOrderDeleteQueue object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger_factory,
    TimeInterface $time
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->loggerFactory = $logger_factory->get('delete_commerce_order');
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Processing queue items.
    if (!empty($data)) {
      $this->deleteOrder($data);
    }
  }

  /**
   * Batch operation: Delete an order.
   */
  public function deleteOrder($orderIds) {
    $orderStorage = $this->entityTypeManager->getStorage('commerce_order');

    foreach ($orderIds as $orderId) {
      $order = $orderStorage->load($orderId);

      // Delete commerce_payment.
      $paymentEntities = $this->entityTypeManager->getStorage('commerce_payment')->loadByProperties(['order_id' => $order->id()]);

      if (!empty($paymentEntities) && is_array($paymentEntities)) {
        foreach ($paymentEntities as $paymentEntity) {
          if (isset($paymentEntity)) {
            $paymentEntity->delete();
          }
        }
      }
      // Delete orders.
      $order->delete();
      $this->loggerFactory->notice('Commerce Order with ID @orderId deleted.', ['@orderId' => $orderId]);
    }
  }

}
