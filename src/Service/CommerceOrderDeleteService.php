<?php

namespace Drupal\delete_commerce_order\Service;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class commerceOrderDeleteService For bulk operations.
 *
 * @package Drupal\delete_commerce_order\Service
 */
class CommerceOrderDeleteService {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The contruction for the service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactory $loggerFactory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerFactory = $loggerFactory->get('delete_commerce_order');
  }

  /**
   * Initiating the batch process.
   *
   * @param array $data
   *   The data for operations.
   * @param array $batchArray
   *   The batch processing array.
   */
  public function initiateBatchProcessing(array $data, array $batchArray) {

    // Define the operation to be performed in each batch iteration.
    $batch = [
      'title' => $batchArray['title'],
      'init_message' => $batchArray['init_message'],
      'progress_message' => $batchArray['progress_message'],
      'error_message' => $batchArray['error_message'],
      'operations' => [],
      'finished' => [$this, 'finishedCallback'],
    ];

    // Add delete operations to the batch.
    foreach ($data as $nid) {
      $batch['operations'][] = [[$this, 'deleteOrder'], [$nid]];
    }

    // Start the batch process.
    batch_set($batch);
  }

  /**
   * Batch operation: Delete a node.
   */
  public function deleteOrder($orderId, &$context) {
    $orderStorage = $this->entityTypeManager->getStorage('commerce_order');
    $order = $orderStorage->load($orderId);

    // Delete commerce_payment.
    $paymentEntities = $this->entityTypeManager->getStorage('commerce_payment')->loadByProperties(['order_id' => $order->id()]);

    if (!empty($paymentEntities) && is_array($paymentEntities)) {
      foreach ($paymentEntities as $paymentEntity) {
        if (isset($paymentEntity)) {
          $paymentEntity->delete();
        }
      }
    }
    // Delete orders.
    $order->delete();

    $context['results'][] = $orderId;
    $context['message'] = $this->t('Commerce Order with ID @orderId deleted.', ['@orderId' => $orderId]);
    $this->loggerFactory->notice($this->t('Commerce Order with ID @orderId deleted.', ['@orderId' => $orderId]));

  }

  /**
   * Batch finished callback.
   */
  public function finishedCallback($success, $results, $operations) {
    if ($success) {
      $message = 'Record(s) delted successfully.';
      $this->loggerFactory->notice($message);
    }
    else {
      $this->loggerFactory->error('An error occurred during node deletion.');
    }
  }

}
