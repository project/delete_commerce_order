<?php

namespace Drupal\delete_commerce_order\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\delete_commerce_order\Service\CommerceOrderDeleteService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting commerce orders based on month.
 */
class CommerceOrderDeletionForm extends ConfigFormBase {

  /**
   * The Entity type manger.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Date and time formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The Batch processing custom server.
   *
   * @var \Drupal\delete_commerce_order\Service\CommerceOrderDeleteService
   */
  protected $commerceDeteteService;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'delete_commerce_order.settings';

  /**
   * Constructs a new CommerceOrderDeletionForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The Date Formatter Interface.
   * @param \Drupal\delete_commerce_order\Service\CommerceOrderDeleteService $commerce_order
   *   The delete commerce order service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messanger service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, DateFormatterInterface $dateFormatter, CommerceOrderDeleteService $commerce_order, MessengerInterface $messenger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->dateFormatter = $dateFormatter;
    $this->commerceDeteteService = $commerce_order;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('delete_commerce_order.batch_processing_service'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_commerce_order';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['note'] = [
      '#type' => 'markup',
      '#markup' => '<p class= comerce-alert> <b>* Make Sure that you have taken neccsary Database Backup </b></p>',
    ];

    $form['cron_radio'] = [
      '#type' => 'radios',
      '#title' => $this->t('Do you want to set a Periodic cron to delete the orders?'),
      '#options' => [
        '0' => $this->t('No'),
        '1' => $this->t('Yes'),
      ],
      '#default_value' => $config->get('cron_radio') ?? 0,
    ];

    // Get today's date.
    $today = $this->dateFormatter->format(time(), 'custom', 'Y-m-d');

    // Add Date element to the form.
    $form['selected_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Select Date'),
      '#description' => $this->t('Please select a past date.'),
      '#default_value' => $config->get('selected_date') ?? $today,
      '#attributes' => [
        'max' => $today,
      ],
      '#prefix' => '<div class = selected-date-identifier>',
      '#suffix' => '</div>',
    ];

    $form['intervel'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose an option'),
      // '#required' => TRUE,
      '#options' => [
        date('Y-m-d', strtotime('-1 month')) => $this->t('Delete Older than 1 Month (@date)', ['@date' => date('Y-m-d', strtotime('-1 month'))]),
        date('Y-m-d', strtotime('-3 months')) => $this->t('Delete Older than 3 Months (@date)', ['@date' => date('Y-m-d', strtotime('-3 months'))]),
        date('Y-m-d', strtotime('-6 months')) => $this->t('Delete Older than 6 Months (@date)', ['@date' => date('Y-m-d', strtotime('-6 months'))]),
        date('Y-m-d', strtotime('-1 year')) => $this->t('Delete Older than 1 Year (@date)', ['@date' => date('Y-m-d', strtotime('-1 year'))]),
        date('Y-m-d', strtotime('-2 years')) => $this->t('Delete Older than 2 Years (@date)', ['@date' => date('Y-m-d', strtotime('-2 years'))]),
        date('Y-m-d', strtotime('-3 years')) => $this->t('Delete Older than 3 Years (@date)', ['@date' => date('Y-m-d', strtotime('-3 years'))]),
        date('Y-m-d', strtotime('-4 years')) => $this->t('Delete Older than 4 Years (@date)', ['@date' => date('Y-m-d', strtotime('-4 years'))]),
        date('Y-m-d', strtotime('-5 years')) => $this->t('Delete Older than 5 Years (@date)', ['@date' => date('Y-m-d', strtotime('-5 years'))]),
      ],
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $config->get('intervel'),
      '#attributes' => [
        'id' => 'edit-intervel',
        'class' => ['intervel-class'],
      ],
      '#prefix' => '<div class = interval-identifier>',
      '#suffix' => '</div>',
    ];

    $form['#attached']['library'][] = 'delete_commerce_order/delte_order';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration.
    $this->config(static::SETTINGS)

    // Set the submitted configuration setting.
      ->set('selected_date', $form_state->getValue('selected_date'))
      ->set('cron_radio', $form_state->getValue('cron_radio'))
      ->set('intervel', $form_state->getValue('intervel'))
      ->save();

    $cronRadio = $form_state->getValue('cron_radio');

    if ($cronRadio == '0') {

      $date = $form_state->getValue('selected_date');

      // Define the operation to be performed in each batch iteration.
      $batch = [
        'title' => 'Deleting Commerce Order.....',
        'init_message' => 'Starting to delete Commerce Order...',
        'progress_message' => 'Processed @current out of @total.',
        'error_message' => 'An error occurred during order deletion.',
      ];

      // Get commerce order entities for the selected month and delete them.
      $query = $this->entityTypeManager->getStorage('commerce_order')->getQuery();
      $query->condition('created', strtotime($date), '<');
      $query->accessCheck(FALSE);
      $orderIds = $query->execute();

      if (!empty($orderIds)) {
        $this->commerceDeteteService->initiateBatchProcessing($orderIds, $batch);
        $this->messenger->addError("Batch Process executed.");
      }
      else {
        $this->messenger->addError("No orders found to delete.");
      }

    }
    else {
      $this->messenger->addMessage("The configuration for the cron is saved.");
    }
  }

}
