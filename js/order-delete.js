(function ($, Drupal, once) {
  Drupal.behaviors.editCronRadio = {
    attach: function (context, settings) {
      $('.comerce-alert').css({ 'color': 'red' });

      // Fetch the initial value of the radio button group.
      var setCron = $('input[name="cron_radio"]:checked').val();
      var confirmationMessage = '';

      $(once('edit-cron-radio', 'input[type="radio"]', context)).on('click', function () {
        if ($(this).val() == 1) {
          setCron = '1';
        } else {
          setCron = '0';
        }

        // Initial visibility setup based on the initial value of setCron.
        if (setCron == '1') {
          confirmationMessage = "Are you sure you want to delete the records via cron?";
          $('.selected-date-identifier', context).hide();
          $('.interval-identifier', context).show();
        } else {
          confirmationMessage = "Are you sure you want to delete the records via batch?";
          $('.selected-date-identifier', context).show();
          $('.interval-identifier', context).hide();
        }
      });

      // Get Default Cron.
      if (setCron == '1') {
        confirmationMessage = "Are you sure you want to delete the records via cron?";
        $('.selected-date-identifier', context).hide();
        $('.interval-identifier', context).show();
      } else {
        confirmationMessage = "Are you sure you want to delete the records via batch?";
        $('.selected-date-identifier', context).show();
        $('.interval-identifier', context).hide();
      }

      // Confirmation message.
      once('editCronRadio', '#edit-submit', context).forEach(function (element) {
        element.addEventListener('click', function (e) {
          var selectedValue = $('#edit-intervel').val();

          // Interval Validation.
          if (setCron == '1' && !(selectedValue)) {
            alert("Please select Time Interval from select Box.");
            e.preventDefault();
            return false;
          }
          var confirmation = confirm(confirmationMessage);
          if (!confirmation) {
            e.preventDefault();
          }
        });
      });
    }
  };
})(jQuery, Drupal, once);
