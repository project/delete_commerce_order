# Commerce Order Bulk Delete

Commerce Order Bulk Delete intends to provide the users to delete the Order,
information, via batch processing or via Queue worker.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/delete_commerce_order).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/
delete_commerce_order).

Note: Please make sure that you have taken backup of your Data Base before,
enabling the module. It is recommended to uninstall the module,
if you are not using it.

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

- [Commerce Core](https://www.drupal.org/project/commerce)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/
installing-drupal-modules).

## Configuration

Configure the admin toolbar tools at (/admin/commerce/order-deletion).

## Maintainers

Current maintainers:

- [Prabu (PrabuEla)](https://www.drupal.org/u/prabuela)

Supporting organizations:

- [Specbee](https://www.drupal.org/specbee) Sponsored the module development
